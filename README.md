# doc-templates

Documentation templates for DSA-2000 project documents. Templates are
sorted into sub-directories by the document creation toolchain they
require.

The only supported toolchain presently is [pandoc](https://pandoc.org).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/planning/doc-templates. Instructions for
cloning the code repository can be found on that site. To provide
feedback (*e.g* bug reports or feature requests), please see the issue
tracker on that site. If you wish to contribute to this project,
please read [CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.

# Document templates for pandoc

## LaTeX

Use `template.latex` for conversion by `pandoc` to LaTeX or PDF
formats using the `--template` command line option.

The template uses a PNG file for the DSA-2000 logo, named
`d2k-logo.png`, which is included in the directory in which the
template file exists. See below for important instructions about
setting the path to this PNG file when using `pandoc`.

Several variables are referenced by the LaTeX template file, which
provide document metadata. These variables are usually provided in a
YAML header that your document defines. As an example, here is the
YAML header for the `symcam` design documentation

``` yaml
---
title: RCP imaging pipeline design
author:
- Martin Pokorny
abstract: |
  We describe the design of the RCP imaging pipeline software. This
  document is a snapshot of the design section of the RCP project
  documentation at
  [https://dsa-2000.gitlab.io/code/rcp/rcpl/symcam](https://dsa-2000.gitlab.io/code/rcp/rcpl/symcam)
  for the `symcam` software release v20231006.0. Readers should refer
  to the web site for documentation that is updated with every release
  of the `symcam` software.
document-number: 00025
wbs-level-2: Radio Camera Processor
wbs-level-2-abbrev: RCP
document-type: Design Report
document-type-abbrev: DES
toc-depth: 5
revisions:
- version: 1
  date: 2023-09-28
  remarks: v20230928.0
  authors:
  - Martin Pokorny
- version: 2
  date: 2023-10-06
  sections:
  - Events
  - Determinism
  remarks: v20231006.0
  authors:
  - Martin Pokorny
...
```

The `title`, `author`, `document-number`, `wbs-level-2`,
`wbs-level-2-abbrev`, `document-type`, `document-type-abbrev`, and
`revisions` variables are required. `abstract` and `toc-depth` are
optional. Additionally, if the `is-controlled` variable is defined
with any value, the document signoff table is created on the title
page (this still needs work). Note that the `author` variable value is
a list, despite what is implied by the variable name.

For each item of the `revisions` list:
- the `authors` field value is a list
- the `sections` field is optional, and works as follows:
  - if present, the value is a list of section header identifiers
    (depending on `pandoc` options used, you may have to define these
    explicitly, or not)
  - if missing, then the text "all" is inserted into "sections
  affected" column of the revisions table.

The generated document will include a PNG for the DSA-2000 logo if the
path to the PNG file is given as the value of a `pandoc` variable
named `logo`. When that variable is not defined, the logo in the PDF
is replaced with the text "DSA-2000". The examples below illustrate
one way to define the variable on the `pandoc` command line.

Usage examples for this template:

### Interactive version

``` shell
pandoc frontmatter.yml a-file.md b-file.md -o doc.pdf -s \
       -V colorlinks \
       -V logo=path/to/doc-templates/pandoc/d2k-logo.png \
       --template path/to/doc-templates/pandoc/template.latex
```
### Print version

``` shell
pandoc frontmatter.yml a-file.md b-file.md -o doc.pdf -s \
       -V colorlinks -V links-as-notes \
       -V logo=path/to/doc-templates/pandoc/d2k-logo.png \
       --number-sections \
       --template path/to/doc-templates/pandoc/template.latex
```
